# I love LiGNUx

![](https://i.postimg.cc/52QqC2kg/I-love-lignux-infos.png)

## Stickers
Format de fichiers: .pdf/X-3:2003 / .svg / .png
- 2 couleurs (tons directs)
- Taille: 50x70mm

## Round
Format de fichiers: .pdf/X-3:2003 / .svg / .png
- 2 couleurs (tons directs)
- Taille: 50x50mm


### Infos
Auteur  : https://maly.io/@Troll  
Source  : https://maly.io/@Troll/101070277549303037  
Licence : [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)


